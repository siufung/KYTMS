package com.kytms.transportorder.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 陈小龙
 * 分段订单
 *  2018-03-23
 */
public interface LedDao<Led> extends BaseDao<Led> {
}
